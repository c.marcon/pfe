(* License GPL V3 englobe MIT et CECIL-B *)


Set Warnings "-notation-overridden, -parsing, -masking-absolute-name".


Require Import AbstractBigraphs.
Require Import Names.
Require Import SignatureBig.
Require Import Equality.
Require Import Bijections.
Require Import MyBasics.
Require Import ParallelProduct.
Require Import MergeProduct.
Require Import Nesting.
Require Import MathCompAddings.


Require Import FunctionalExtensionality.
Require Import ProofIrrelevance.
Require Import Coq.Lists.List.
From RegexpBrzozowski Require Import regexp.


Require Import Coq.Program.Wf.


From mathcomp Require Import all_ssreflect.

Import ListNotations.

Module SortingBig (s : SignatureParameter) (n : NamesParameter).

Module MB := MergeBig s n.
Include MB. 



Section SortBigraphs.
(*atomic node = has no children = is not an image through parent (neither through a site nor a node) *)
Definition not_is_atomic {s i r o} {b:bigraph s i r o} (n: get_node b) : bool := 
  Coq.Lists.List.existsb 
    (A := ordinal s)
    (fun s => match (get_parent (bg:=b)) (inr s) with 
      |inr _ => false 
      |inl n' => n == n'
      end) 
    (enum (ordinal s))
  || 
  Coq.Lists.List.existsb 
    (A := get_node b)
    (fun nod => match (get_parent (bg:=b)) (inl nod) with 
      |inr _ => false 
      |inl n' => n == n'
      end) 
    (enum ((get_node b))).


(*this supposes we give in l the list of nodes with the correct mysort
it checks whether all elements of the list is atomic *)
(* Fixpoint check_atomic {s i r o} {b:bigraph s i r o} (l:list (get_node b)) :=
  match l with 
    | [] => true 
    | nh::nq => (negb (not_is_atomic (b:=b) nh)) && check_atomic nq
    end. *)


Definition get_children {s i r o} {b:bigraph s i r o} (n: get_node b) : list (get_node b) :=
  filter 
    (fun nh => 
      match (get_parent (bg:=b)) (inl nh) with 
      |inr _ => false
      |inl n' => n == n'
      end) 
    (enum (get_node b)).
    
Definition check_one_child {s i r o} {b:bigraph s i r o} (n: get_node b) : bool := 
  size (get_children n) == 1.




Parameter mysort:orderType tt. (*pourquoi tt et pas autre chose?*)
Parameter DefautltSort:mysort.
Parameter EqDecS : forall x y : mysort, {x = y} + {x <> y}. (*TODO not a parameter maybe it can be derived from orderType*)
Parameter relSort: rel mysort. (*TODO same*)



Class Monad (M : Type -> Type) := {
  ret : forall {A}, A -> M A;
  bind : forall {A B}, M A -> (A -> M B) -> M B
}.

Inductive pat : Type :=
  | void_pat
  | eps_pat
  | and_pat (p:pat) (p':pat)
  | or_pat (p:pat) (p':pat)
  | star_pat (p : pat)
  | baseS_pat (s:mysort).

Record term : Type := 
  Term {fixedpart : list mysort ; optpart : list (list mysort)}.

Inductive LSigma (A : Type) : Type :=
  SigRet : list A -> LSigma A.

(* Instance Monad_list : Monad list := {
  ret := fun A x => [x]; 
  bind := fun A B (l:seq A) (f:A -> seq B) => concat (map f l)
}.   *)

Arguments SigRet {A} .
Arguments ret {M _ A} _.
Arguments bind {M _ A B} _ _.

Instance Monad_Sigma : Monad LSigma := {
  ret := fun A x => SigRet [x] ;
  bind := fun A B (ls:LSigma A) (f:A -> LSigma B)  =>
    match ls with
    | SigRet l => SigRet(concat (map (fun x => match f x with SigRet y => y end) l))
    end
}.


Definition consM (x : term) (xs : LSigma term) : LSigma term :=
  match xs with | SigRet lxs => SigRet (x::lxs) end.


Definition consM' (x : term) (xs : LSigma term) : LSigma term :=
  bind (ret x) 
    (fun t => match xs with | SigRet xs' => SigRet (x::xs')end ).


  (* \sigma w (w1 + ... + wm)*  *)
Definition flat_pat : Type := LSigma term.
Definition deflatten (fp : flat_pat) : pat. Admitted. 


Fixpoint star_a_pattern (lt:list term) : flat_pat := 
  match lt with
  | [] => SigRet []
  | Term s [] :: q => 
    let std_s := sort relSort s in 
    consM (Term [] [std_s]) (star_a_pattern q)
  | Term s ls :: q => 
    let std_s := sort relSort s in 
    let std_ls := map (fun sl => sort relSort sl) ls in 
    consM (Term [] [std_s])  
      (consM (Term std_s (std_s::std_ls)) 
        (star_a_pattern q))
  end.



Fixpoint and_two_pattern (lt1:list term) (lt2:list term) : flat_pat := 
  match lt2 with
  | [] => SigRet []
  | Term s2 ls2 :: q => 
    match and_two_pattern lt1 q with 
    | SigRet q1 =>
      SigRet (map (fun x => 
        match x with 
        | Term s1 ls1 => 
          let std_s1s2 := sort relSort (s1++s2) in 
          let std_ls1ls2 := map (fun sl => sort relSort sl) (ls1++ls2) in 
            Term (std_s1s2) (std_ls1ls2) 
        end) 
        lt1 
        ++ q1)
    end
  end.

(* Search foo. *)
Fixpoint flatten (pattern:pat) : flat_pat :=
  match pattern with
  | void_pat => SigRet [] 
  | eps_pat => SigRet [] 
  | and_pat p p' => 
  let fp := match flatten p with | SigRet fp => fp end in 
  let fp' := match flatten p' with | SigRet fp' => fp' end in 
  and_two_pattern fp fp'
  | or_pat p p' => 
    let fp := match flatten p with | SigRet fp => fp end in 
    let fp' := match flatten p' with | SigRet fp' => fp' end in 
    SigRet (fp ++ fp') (*retrouve les mêmes éléments dans flatten p et flatten p' *)
  | star_pat p => 
    let fp := match flatten p with | SigRet fp => fp end in 
    star_a_pattern (fp)
  | baseS_pat s => ret (Term [s] [])
  end.

(*flatten p est sorted*)


(* Example ams : mysort := "a"%string.
Example bms : mysort := "b"%string.
Example cms : mysort := "c"%string.
Example dms : mysort := "d"%string.
Example ems : mysort := "e"%string.
Example mypattern : pat := 
  and_pat 
    (or_pat (sname ams) (sname bms)) 
    (or_pat 
      (and_pat (sname cms) (sname dms))
      (sname ems)).

Compute (flatten mypattern). *)



(*missing the LinkGraph aspect*)
Inductive constructor : Type :=
  (* | ctrl_name (c:Kappa) I'm pretty sure this is no longer needed from the void_pat *)
  | patterns (c: Kappa) (p:pat).

Inductive formation_rule : Type := 
  | sort_rule (s:mysort) (constructors : list constructor).


Inductive flat_formation_rule : Type := 
  | flat_sort_rule (s:mysort) (constructors : list constructor).



Parameter signatureK: Kappa -> mysort.

Definition check_one_child_of_sort_s {s i r o} {b:bigraph s i r o} (s: mysort) (n: get_node b): bool :=
  match get_children n with 
  | child :: [] => EqDecS (signatureK (get_control child)) s
  | _ => false
  end.

Fixpoint check_nodes_of_sort_s {s i r o} {b:bigraph s i r o} (s: mysort) (l: list (get_node b)): bool :=
  match l with 
  | child :: q => EqDecS (signatureK (get_control child)) s && check_nodes_of_sort_s s q
  | [] => true
  end. 




Fixpoint derive_no_fixed_part (s:mysort) (l:list (list mysort)) 
    : list term := 
    match l with 
    | [] => []
    | lo :: qlo => 
      match lo with 
      | [] => derive_no_fixed_part s qlo
      | so :: qo => 
        if so == s then (Term qo (qo::qlo)) :: derive_no_fixed_part s qlo (*est ce que je suis sûre de moi sur la première lettre?*)
        else derive_no_fixed_part s qlo
      end
    end.



(*utiliser la mesure en fuel +
fonction aux qui derive un mot*)
Definition derive (s:mysort) (p:term) 
    : list term := 
  match fixedpart p with
  | [] => derive_no_fixed_part s (optpart p)
  | sp :: qp => if sp == s then [Term qp (optpart p)] 
    else map 
      (fun t => match t with | Term fp op => Term (sort relSort (sp::qp++fp)) op end) 
      (derive_no_fixed_part s (optpart p))
  end.



(* Theorem der_in_derive : der x e \in derive s p. *)


Fixpoint check_pattern_term (p:term) (l:list mysort) : bool := 
  match l with (*sort relSort l*) 
  | [] => fixedpart p == []
  | s::qs => 
    fold_left 
      (fun qt t => orb (check_pattern_term t qs) qt)
      (derive s p)
      false
  end. (* 
    if s in fixed part then check_pattern_term (derive s p) qs 
    else if dans option then check if all option is in  
    else false  *)



Fixpoint check_pattern_term_faster (p:term) (l:list mysort) : bool := 
  match l with (* sort relSort l *)
  | [] => fixedpart p == []
  | s::qs => 
    match fixedpart p with 
    | [] => 
      fold_left 
        (fun qt t => orb (check_pattern_term_faster t qs) qt)
        (derive s p)
        false
    | sfp::qfp => if s == sfp then 
        check_pattern_term_faster (Term qfp (optpart p)) qs
        else 
          fold_left 
            (fun qt t => orb (check_pattern_term_faster t qs) qt)
            (derive s p)
            false
    end
  end. 

Lemma fold_left_id {bo te:Type} : forall l:list te, forall t f f', f = f' -> @fold_left bo te f l t = fold_left f' l t.
intros. subst f. reflexivity. Qed.

Lemma check_pattern_term_faster_eq_check_pattern_term (p:term) (l:list mysort) :
  check_pattern_term_faster p l = check_pattern_term p l.
  Proof.
    unfold check_pattern_term, check_pattern_term_faster.
    auto. 
    induction l.
    - auto.
    - auto. destruct p as [fp op]. simpl.
    destruct fp.
    + apply fold_left_id.
    apply functional_extensionality.
    intros bo.
    apply functional_extensionality.
    intros te.
    destruct bo eqn:BI.
    ++ rewrite (Bool.orb_true_r (check_pattern_term te l)).
    rewrite (Bool.orb_true_r).
    reflexivity.
    ++ 
    rewrite (Bool.orb_false_r).
    rewrite (Bool.orb_false_r).
    Abort.

(* Fixpoint check_pattern_term_in_dec (p:term) (l:list mysort) : bool := 
  match l with 
  | [] => fixedpart p == []
  | s::qs => 
      if in_dec EqDecS s (fixedpart p) then 
         match derive s p with 
         | [] => false e Algebra
         | t::qt => check_pattern_term_in_dec t qs 
         end
        else false
  end.  *)
  (* 
    if s in fixed part then check_pattern_term (derive s p) qs 
    else if dans option then check if all option is in  
    else false  *)

Definition check_pattern (fp:flat_pat) (l:list mysort)  : bool := 
  let fpl := match fp with | SigRet fp => fp end in   
  fold_left 
    (fun qt p => orb (check_pattern_term p l) qt)
    fpl 
    false.
  (* match fp with 
  | [] => false
  | s::qs => orb (check_pattern_term s l) (check_pattern qs l)
  end.  *)

Definition get_nodes_control_k {s i r o} (b:bigraph s i r o) (k:Kappa) : list (get_node b) :=
  filter  
    (fun nh => EqDecK (get_control (bg:=b) nh) k) 
    (enum (get_node b)).

Fixpoint get_children_list {s i r o} {b:bigraph s i r o} (l:list (get_node b)) : list (list (get_node b)):=
  match l with 
  |[] => []
  |n::qn => get_children n :: (get_children_list qn)
  end.

Fixpoint get_children_sorts_list {s i r o} {b:bigraph s i r o} (l:list (get_node b)) : list (list mysort):=
  match l with 
  |[] => []
  |n::qn => (map (fun n => signatureK (get_control (bg:=b) n)) (get_children n)) :: (get_children_sorts_list qn)
  end.

Definition check_constructor {s i r o}
  (b:bigraph s i r o) (s:mysort) (c:constructor) : bool :=
  match c with
  (* | ctrl_name k =>
    fold_left 
      (fun qt nh => (negb (not_is_atomic (b:=b) nh)) && qt)
      (get_nodes_control_k b k) 
      true *)
  | patterns k p => 
    let llschildren := get_children_sorts_list (get_nodes_control_k b k) in
    fold_left 
      (fun qt lschildren => check_pattern (flatten p) lschildren && qt)
      llschildren
      true
  end.


Definition check_list_constructor {s i r o}
  (b:bigraph s i r o) (s:mysort) (clist:list constructor) : bool :=
  fold_left 
    (fun qt c => check_constructor b s c && qt)
    clist
    true.

(* Fixpoint check_list_constructor {s i r o}
  (b:bigraph s i r o) (s:mysort) (clist:list constructor) : bool :=
  match clist with
    | [] => true
    | hclist :: qclist => 
      match hclist with
      | ctrl_name c =>
        EqDecS (signatureK c) s && 
        fold_left 
          (fun qt nh => (negb (not_is_atomic (b:=b) nh)) && qt)
          (filter  (fun nh => EqDecK (get_control (bg:=b) nh) c) (enum (get_node b))) 
          true
      | patterns c p => 
        check_pattern 
          (flatten p) 
          (map 
            (fun n => signatureK (get_control (bg:=b) n))
            (get_children_list 
              (filter (fun nh => EqDecK (get_control (bg:=b) nh) c) (enum (get_node b)))))
      end 
      && check_list_constructor b s qclist
    end. *)


Definition check_formation_rule {s i r o}
    (b:bigraph s i r o) (f:formation_rule) : bool := 
  match f with 
  | sort_rule s clist => check_list_constructor b s clist    
  end.




End SortBigraphs.

End SortingBig.