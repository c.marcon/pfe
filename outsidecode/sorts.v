From Coq Require Import List Setoid.
Import ListNotations.
From Coq Require Import Lia.
From Coq Require Import Recdef.

(** ** Mechanization of sort expressions from "A bigraph paper of sorts" *)

(** Sort expressions *)

Parameter name : Type.
Axiom name_eq_dec : forall (x y: name), {x = y} + {x <> y }.
(* Proof. *)
(*   unfold name. destruct name_fin, class, eqtype_hasDecEq_mixin; simpl. *)
(*   intros x y. *)
(*   destruct (eq_op x y) eqn:EQ. *)
(*   - rewrite <-Bool.reflect_iff in EQ; [|eauto]; auto. *)
(*   - right. intros NEQ. rewrite Bool.reflect_iff in NEQ; [|eauto]; congruence. *)
(* Defined. *)

Infix "=?" := name_eq_dec (at level 50).

Inductive sort_exp :=
| Zero
| One
| Name (x: name)
| Sum (e1 e2 : sort_exp)
| Prod (e1 e2 : sort_exp)
| Star (e1 : sort_exp).

(* [s]^[n] *)
Fixpoint product s n :=
  match n with
  | O => One
  | S n => Prod s (product s n)
  end.

(* Sets as characteristic functions *)
Definition set (A : Type) := A -> Prop.
Definition set_In {A: Type} (x : A) (s: set A) := s x.
Infix "∈" := set_In (at level 50).

Definition set_equiv {A: Type} (s1 s2: set A) := forall x, x ∈ s1 <-> x ∈ s2.
Infix "≡" := set_equiv (at level 50).

Definition set_empty {A} : set A := fun _ => False.
Notation "∅" := set_empty.

Definition set_singleton {A} (x: A) : set A :=
  fun y => y = x.
Notation "{ X }" := (set_singleton X).

Definition set_union {A} (s1 s2 : set A) :=
  fun x => s1 x \/ s2 x.
Infix "∪" := set_union (at level 45).

Definition set_inter {A} (s1 s2 : set A) :=
  fun x => s1 x /\ s2 x.
Infix "∩" := set_inter (at level 40).

Definition set_cartesian {A1 A2} (s1 : set A1) (s2 : set A2) : set (A1 * A2) :=
  fun '(x1, x2) => s1 x1 /\ s2 x2.

Global Hint Unfold set_In set_empty set_inter set_cartesian : set.

(* Multisets (computable) *)
Definition mset (A : Type) := A -> nat.

Definition mset_equiv {A: Type} (m1 m2: mset A) :=
  forall x, m1 x = m2 x.
Infix "≡n" := mset_equiv (at level 50).

Global Instance mset_equiv_refl {A} : Reflexive (@mset_equiv A).
Proof. now intros ??. Qed.

Definition mset_empty {A} : mset A := fun _ => 0.
Notation "∅n" := mset_empty.

Definition mset_singleton (x : name) : mset name :=
  fun y => if y =? x then 1 else 0.
Notation "{ X }n" := (mset_singleton X).

Definition mset_union {A} (m1 m2 : mset A) : mset A :=
  fun x => m1 x + m2 x.
Infix "∪n" := mset_union (at level 45).

(* [s1] * [s2] *)
Definition cartesian_mset_union {A} (s1 s2 : set (mset A)) : set (mset A) :=
  fun m => exists m1 m2, (m1, m2) ∈ (set_cartesian s1 s2) /\ m ≡n mset_union m1 m2.

Global Hint Unfold mset_equiv mset_empty mset_singleton mset_union : mset.
Global Hint Unfold cartesian_mset_union : mset.

(* [s1]^[n] *)
Fixpoint cartesian_mset_union_n {A} (s1 : set (mset A)) (n : nat) : set (mset A) :=
  match n with
  | 0 => fun m => m ≡n ∅n
  | S n => cartesian_mset_union s1 (cartesian_mset_union_n s1 n)
  end.

(** Semantics of sort expressions *)

Fixpoint sem_sort (e: sort_exp) : set (mset name) :=
  match e with
  | Zero => ∅
  | One => fun m => m ≡n ∅n
  | Name x => fun m => m ≡n { x }n
  | Sum e1 e2 => fun m => m ∈ (sem_sort e1) \/ m ∈ (sem_sort e2)
  | Prod e1 e2 => cartesian_mset_union (sem_sort e1) (sem_sort e2)
  | Star e1 => fun m => exists n, m ∈ cartesian_mset_union_n (sem_sort e1) n
  end.

Global Instance set_equiv_refl {A} : Reflexive (@set_equiv A).
Proof.
  intros ??. reflexivity.
Qed.

Global Instance set_equiv_sym {A} : Symmetric (@set_equiv A).
Proof.
  intros ?? EQ ?. symmetry. apply EQ.

Qed.

Global Instance set_equiv_trans {A} : Transitive (@set_equiv A).
Proof.
  intros ??? EQ1 EQ2 ?. etransitivity; eauto.
Qed.

Lemma set_empty_spec {A} : forall (x: A),
    x ∈ ∅ <-> False.
Proof. now unfold set_empty, set_In. Qed.

Lemma set_union_spec {A} : forall (x: A) s1 s2,
    x ∈ s1 ∪ s2 <-> x ∈ s1 \/ x ∈ s2.
Proof. now unfold set_union, set_In. Qed.

Lemma set_inter_spec {A} : forall (x: A) s1 s2,
    x ∈ s1 ∩ s2 <-> x ∈ s1 /\ x ∈ s2.
Proof. now unfold set_inter, set_In. Qed.

(** Some properties *)

(* Another way of defining the star, closer to the paper *)
Lemma sem_star2 : forall p1,
    sem_sort (Star p1) ≡ fun m => exists n, m ∈ sem_sort (product p1 n).
Proof.
  intros * ?. simpl. unfold set_In.
  split; intros (n&IN); exists n.
  - revert x IN.
    induction n; intros; simpl in *; auto.
    unfold cartesian_mset_union, set_cartesian in *.
    destruct IN as (m1&m2&(?&?)&?); subst.
    exists m1, m2. repeat split; auto.
  - revert x IN.
    induction n; intros; simpl in *; auto.
    unfold cartesian_mset_union, set_cartesian in *.
    destruct IN as (m1&m2&(?&?)&?); subst.
    exists m1, m2. repeat split; auto.
Qed.

Fact cartesian_mset_union_morph {A} : forall (s1 s1' s2 s2': set (mset A)),
    s1 ≡ s1' -> s2 ≡ s2' -> cartesian_mset_union s1 s2 ≡ cartesian_mset_union s1' s2'.
Proof.
  intros * EQ1 EQ2.
  intros ?. autounfold with set mset.
  split; intros (?&?&(M1&M2)&?); do 2 esplit; repeat split; auto.
  all:try eapply EQ1; try eapply EQ2; eauto.
Qed.

Fact cartesian_mset_union_n_morph {A} : forall n (s1 s1' : set (mset A)),
    s1 ≡ s1' -> cartesian_mset_union_n s1 n ≡ cartesian_mset_union_n s1' n.
Proof.
  induction n; intros * EQ1; simpl.
  - reflexivity.
  - intros ?. autounfold with set mset.
    split; intros (?&?&(M1&M2)&?); do 2 esplit; repeat split; auto.
    all:try eapply EQ1; try eapply IHn; eauto.
    intros ?. symmetry. apply EQ1.
Qed.

Lemma sem_sort_morph : forall s,
    forall m1 m2, m1 ≡n m2 -> m1 ∈ sem_sort s -> m2 ∈ sem_sort s.
Proof.
  induction s; intros * EQ In; simpl in *; autounfold with set mset in *.
  - (* zero *) auto.
  - (* one *)
    intros ?; rewrite <-EQ; eauto.
  - (* name *)
    intros ?; rewrite <-EQ; eauto.
  - (* sum *)
    destruct In; eauto.
  - (* prod *)
    destruct In as (?&?&(?&?)&?).
    repeat esplit; eauto.
    intros. rewrite <-EQ; eauto.
  - (* star *)
    destruct In as (n&In). exists n.
    induction n; simpl in *.
    + intros ?. rewrite <-EQ; eauto.
    + autounfold with mset in *. destruct In as (?&?&?&?).
      repeat (esplit; eauto).
      intros. rewrite <-EQ; eauto.
Qed.

(** Compatibility of two sorts *)

Definition compat (s1 s2: sort_exp) :=
  (sem_sort s1 ≡ sem_sort s2) \/ not (sem_sort s1 ∩ sem_sort s2 ≡ ∅).
Infix "><" := compat (at level 50).

Definition compat' (s1 s2: sort_exp) :=
  (sem_sort s1 ≡ ∅ /\ sem_sort s2 ≡ ∅) \/ not (sem_sort s1 ∩ sem_sort s2 ≡ ∅).

From Coq Require Import Classical.

(* Uses classical logic *)
Fact empty_or_not {A} : forall (s: set (mset A)),
    (forall m, ~m ∈ s) \/ (exists m, m ∈ s).
Proof.
  intros.
  destruct (classic (exists m, m ∈ s)); auto.
  left. intros. contradict H. eauto.
Qed.

Lemma compat_compat' : forall s1 s2,
    s1 >< s2 <-> compat' s1 s2.
Proof.
  intros *; split; (intros [C|C]; [|right]; auto).
  - destruct (empty_or_not (sem_sort s1)) as [Forall|(m&Ex)].
    + left. autounfold with set in *.
      split; intros m; (split; [intros IN|intros []]).
      * apply Forall in IN as [].
      * apply C, Forall in IN as [].
    + right. intros CONTRA.
      apply (CONTRA m). split; auto. now apply C.
  - destruct C as (EQ1&EQ2). left.
    etransitivity; eauto. now symmetry.
Qed.

(** Brzozowski derivative for commutative regex *)

Fixpoint derivative (a: name) (s: sort_exp) :=
  match s with
  | Zero => Zero
  | One => Zero
  | Name x => if x =? a then One else Zero
  | Prod s1 s2 => Sum (Prod (derivative a s1) s2) (Prod s1 (derivative a s2))
  | Sum s1 s2 => Sum (derivative a s1) (derivative a s2)
  | Star s => Prod (derivative a s) (Star s)
  end.

(* Derive a set-of-multiset *)
Definition derive a (s: set (mset name)) :=
  fun m => s (m ∪n { a }n).
Global Hint Unfold derive : set.

Lemma derive_In : forall a s m,
    (forall m1 m2, m1 ≡n m2 -> m1 ∈ s -> m2 ∈ s) ->
    m ∈ s ->
    m a > 0 ->
    (fun x => if x =? a then m a - 1 else m x) ∈ (derive a s).
Proof.
  intros * Recept In NotZero.
  autounfold with set mset.
  eapply Recept, In.
  intros ?. destruct (x =? a); auto. subst. lia.
Qed.

(* Lemma derive_nIn : forall a s m, *)
(*     (forall m1 m2, m1 ≡n m2 -> m1 ∈ s -> m2 ∈ s) -> *)
(*     m ∈ s -> *)
(*     m a = 0 -> *)
(*     (fun x => if x =? a then 0 else m x) ∈ (derive a s). *)
(* Proof. *)
(*   intros * Recept In NotZero. *)
(*   autounfold with set mset. *)
(*   eapply Recept, In. *)
(*   intros ?. destruct (x =? a); subst; auto. lia. *)
(* Qed. *)

Fact cartesian_mset_union_n_0 : forall s n (a: name),
    (forall m, m ∈ s -> m a = 0) ->
    forall m1, m1 ∈ (cartesian_mset_union_n s n) -> m1 a = 0.
Proof.
  induction n; intros * Zero ? In; simpl in *; autounfold with set mset in *.
  - (* 0 *) auto.
  - (* S *)
    destruct In as (?&?&(M1&M2)&EQ). rewrite EQ.
    eapply IHn in M2; eauto. rewrite Zero, M2; auto.
Qed.

Lemma derivative_correct : forall a s,
    sem_sort (derivative a s) ≡ derive a (sem_sort s).
Proof.
  induction s; simpl.
  - (* zero *) reflexivity.
  - (* one *)
    intros ?; simpl. autounfold with set mset; simpl.
    split; [intros []|intros H].
    specialize (H a). destruct (a =? a); [lia|congruence].
  - (* name *)
    destruct (x =? a); subst; intros ?; simpl; autounfold with set mset.
    + (* eq *)
      split; intros H x'; specialize (H x'); lia.
    + (* neq *)
      split; [intros []|intros H].
      specialize (H a) as Ha; destruct (a =? a), (a =? x); try congruence. lia.
  - (* sum *)
    intros ?; simpl. specialize (IHs1 x). specialize (IHs2 x).
    autounfold with set. now rewrite IHs1, IHs2.
  - (* prod *)
    intros ?; simpl.
    autounfold with set mset.
    split; [intros [(?&?&(H1&?)&?)|(?&?&(?&H2)&?)]|intros (?&?&(S1&S2)&?)]; subst.
    + rewrite (IHs1 x0) in H1; autounfold with set mset in H1.
      repeat (esplit; eauto).
      intros ?; rewrite H0. lia.
    + rewrite (IHs2 x1) in H2; autounfold with set mset in H2.
      repeat (esplit; eauto).
      intros ?; rewrite H0. lia.
    + destruct (x0 a) eqn:M0.
      * destruct (x1 a) eqn:M1.
        1:{ exfalso; specialize (H a).
            destruct (a =? a); try congruence; try lia. }
        right. repeat (esplit; eauto).
        eapply derive_In with (a:=a) in S2; try lia; eauto using sem_sort_morph.
        apply IHs2 in S2; eauto.
        intros y; specialize (H y); simpl in *. destruct (y =? a); subst; try lia.
      * left. repeat (esplit; eauto).
        eapply derive_In with (a:=a) in S1; try lia; eauto using sem_sort_morph.
        apply IHs1 in S1; eauto.
        intros y; specialize (H y); simpl in *. destruct (y =? a); subst; try lia.
  - (* star *)
    intros ?. autounfold with set mset in *.
    split; [intros (m1&?&(S1&n&S2)&EQ)|intros (n&?)].
    + assert (sem_sort s (fun x => m1 x + (if x =? a then 1 else 0))) as S1'.
      { apply IHs; auto. }
      exists (S n). simpl. autounfold with mset.
      repeat (esplit; eauto).
      intros ?. rewrite EQ. lia.
    + destruct n; intros; simpl in *; autounfold with mset in *.
      * specialize (H a). destruct (a =? a); try congruence; try lia.
      * destruct H as (m1&m2&(S1&S2)&EQ).
        destruct (m1 a) eqn:M1.
        -- assert (m2 a > 0) by (specialize (EQ a); destruct (a =? a); subst; try congruence; lia).
           (* (* repeat (esplit; eauto). *) *)
           (* (* ++ eapply sem_sort_morph, IHs. *) *)
           (* (*    2:{ autounfold with set. *) *)
           (* (*        eapply sem_sort_morph, S1. *) *)
           (* (*        instantiate (1:=fun x => if x =? a then _ else _); simpl. *) *)
           (* (*        intros y. destruct (y =? a); subst. *) *)
           (* assert ((forall m, sem_sort s m -> m a = 0) \/ (exists m, sem_sort s m /\ m a > 0)) as [All|(m1'&S1'&M1')]. *)
           (* { admit. (* Classical logic *) } *)
           (* ++ eapply cartesian_mset_union_n_0 in S2; eauto. lia. *)
           (* ++ eapply derive_In with (a:=a) in S1'; try lia; eauto using sem_sort_morph. *)
           (*    repeat (esplit; eauto). *)
           (*    ** eapply IHs. autounfold with set mset in *. *)
           (*       eapply sem_sort_morph; eauto. intros y. *)
           (*       instantiate (1:=fun x => if x =? a then m1' x - 1 else m1' x); simpl. *)
           (*       destruct (y =? a); subst; try lia. *)
           (*    ** intros y. specialize (EQ y); simpl. *)
           (*       destruct (y =? a); subst; try lia. *)
           (*       --- rewrite M1 in EQ; simpl in EQ. rewrite <-EQ. *)
           (*           admit. *)
           (*       --- simpl in EQ. rewrite EQ. *)
           (*       admit. *)
           (*    specialize (EQ a). destruct (a =? a); try congruence; try lia. *)
           (*    rewrite M1, M2 in EQ. *)
           admit.
        (* Contradiction ? *)
        -- eapply derive_In with (a:=a) in S1; try lia; eauto using sem_sort_morph.
           eapply IHs in S1.
           repeat (esplit; eauto).
           intros; simpl. specialize (EQ x0). destruct (x0 =? a); subst; lia.
Admitted.

(** Some rewriting rules *)
(* TODO some might not be useful *)

Ltac destruct_conjs :=
  match goal with
  | H: exists _, _ |- _ => destruct H
  | H: _ /\ _ |- _  => destruct H
  end.

Ltac destruct_hyp :=
  match goal with
  | _ => destruct_conjs
  | H: _ \/ _ |- _  => destruct H
  end.

Lemma prod_comm : forall s1 s2,
    sem_sort (Prod s1 s2) ≡ sem_sort (Prod s2 s1).
Proof.
  intros * ?; simpl; autounfold with set mset.
  split; intros; repeat destruct_hyp; repeat (esplit; eauto).
  1,2:intros ?; rewrite H0; lia.
Qed.

Lemma prod_distr_sum : forall s1 s2 s3,
    sem_sort (Prod (Sum s1 s2) s3) ≡ sem_sort (Sum (Prod s1 s3) (Prod s2 s3)).
Proof.
  intros * ?; simpl; autounfold with set mset.
  split; intros; repeat destruct_hyp; repeat (esplit; eauto).
  - left; repeat (esplit; eauto).
  - right; repeat (esplit; eauto).
Qed.

Lemma prod_distr_star : forall s1 s2,
    sem_sort (Star (Sum s1 s2)) ≡ sem_sort (Prod (Star s1) (Star s2)).
Proof.
  intros * ?; simpl; autounfold with set mset.
  split; intros; repeat destruct_conjs.
  - revert x H. induction x0; intros; simpl in *.
    + exists ∅n, ∅n. repeat split.
      1,2:(exists 0; simpl; intros ?; reflexivity).
      intros ?; rewrite H; now autounfold with mset.
    + autounfold with set mset in *. repeat destruct_conjs.
      (* specialize (IHx0 _  H1). repeat destruct_conjs. *)
      (* destruct H. *)
      (* * do 2 esplit; repeat split; [exists 1; simpl|exists x0; simpl|]. *)
      (*   -- autounfold with set mset in *. repeat esplit; eauto. *)
      (*   -- intros ?; reflexivity. *)
      (*   -- intros. rewrite H0. *)
      (*   simpl. intros. rewrite H0, H3. *)
      (* repeat (esplit; eauto). *)
      (* intros ?; rewrite H0, H3. *)
      admit.
Admitted.

(** Disjunctive form *)
Inductive no_disj : sort_exp -> Prop :=
| no_disj_zero : no_disj Zero
| no_disj_one : no_disj One
| no_disj_name : forall x, no_disj (Name x)
| no_disj_prod : forall s1 s2, no_disj s1 -> no_disj s2 -> no_disj (Prod s1 s2)
| no_disj_star : forall s, no_disj s -> no_disj (Star s).

Inductive disjunctive : sort_exp -> Prop :=
| disjunct_sum : forall s1 s2, disjunctive s1 -> disjunctive s2 -> disjunctive (Sum s1 s2)
| disjunct_base : forall s, no_disj s -> disjunctive s.

Fixpoint sort_size s :=
  match s with
  | Prod s1 s2 => 1 + sort_size s1 + sort_size s2
  | Sum s1 s2 => 1 + sort_size s1 + sort_size s2
  | Star s => 1 + sort_size s
  | Name _ => 1
  | _ => 0
  end.

Definition sort_pair_size s := sort_size (fst s) + sort_size (snd s).

Function distr_prod s {measure sort_pair_size s} :=
  match s with
  | (Sum s1 s2, s3) => Sum (distr_prod (s1, s3)) (distr_prod (s2, s3))
  | (s1, Sum s2 s3) => Sum (distr_prod (s1, s2)) (distr_prod (s1, s3))
  | (s1, s2) => Prod s1 s2
  end.
Proof.
  all:destruct s; simpl; intros; subst.
  all:unfold sort_pair_size; simpl; lia.
Qed.

Function distr_star s :=
  match s with
  | Sum s1 s2 => distr_prod (distr_star s1, distr_star s2)
  | _ => Star s
  end.

Function make_disjunctive (s: sort_exp) :=
  match s with
  | Sum s1 s2 => Sum (make_disjunctive s1) (make_disjunctive s2)
  | Prod s1 s2 => distr_prod (make_disjunctive s1, make_disjunctive s2)
  | Star s => distr_star (make_disjunctive s)
  | _ => s
  end.

Ltac inv H := inversion H; subst; clear H.

Fact distr_prod_complete : forall p,
    disjunctive (fst p) ->
    disjunctive (snd p) ->
    disjunctive (distr_prod p).
Proof.
  intros *. functional induction distr_prod p; intros * D1 D2; simpl in *.
  - inv D1; [|inv H]. constructor; eauto.
  - inv D2; [|inv H]. constructor; eauto.
  - inv D1; try now exfalso. inv D2; try (destruct s1; now exfalso).
    repeat constructor; auto.
Qed.

Fact distr_star_complete : forall s,
    disjunctive s ->
    disjunctive (distr_star s).
Proof.
  induction s; intros * Disj; inv Disj; subst; simpl.
  all:try now (repeat (constructor; auto)).
  eapply distr_prod_complete; eauto.
Qed.

Lemma make_disjunctive_complete : forall s,
    disjunctive (make_disjunctive s).
Proof.
  induction s; simpl; try now repeat constructor.
  - (* sum *) constructor; auto.
  - (* prod *)
    eapply distr_prod_complete; eauto.
  - (* star *)
    eapply distr_star_complete; eauto.
Qed.

Fact distr_prod_correct : forall p,
    sem_sort (distr_prod p) ≡ sem_sort (Prod (fst p) (snd p)).
Proof.
  intros *. functional induction distr_prod p.
  - intros m. setoid_rewrite (prod_distr_sum _ _ _ m).
    simpl; autounfold with set mset in *.
    rewrite (IHs m), (IHs0 m). firstorder.
  - intros m; specialize (IHs m); specialize (IHs0 m).
    simpl. autounfold with set mset in *.
    rewrite IHs, IHs0. firstorder.
  - reflexivity.
Qed.

Fact distr_star_correct : forall s,
    sem_sort (distr_star s) ≡ sem_sort (Star s).
Proof.
  intros *. functional induction distr_star s.
  - (* prod *)
    intros ?. rewrite (distr_prod_correct _ x), (prod_distr_star _ _ x).
    simpl. autounfold with set mset in *.
    split; intros (?&?&(M1&M2)&?); do 2 esplit; repeat split; eauto.
    1-4:apply IHs0 in M1; apply IHs1 in M2; auto.
  - reflexivity.
Qed.

Lemma make_disjunctive_correct : forall s,
    sem_sort (make_disjunctive s) ≡ sem_sort s.
Proof.
  intros s. functional induction make_disjunctive s.
  - (* sum *)
    intros m; simpl; repeat autounfold with set.
    now rewrite (IHs0 m), (IHs1 m).
  - (* prod *)
    intros m; rewrite (distr_prod_correct _ m).
    now apply cartesian_mset_union_morph.
  - (* star *)
    intros m; rewrite (distr_star_correct _ m).
    simpl; repeat autounfold with set mset.
    split; intros (n&M); exists n; setoid_rewrite (cartesian_mset_union_n_morph _ _ _ _ m); eauto.
    intros ?; symmetry; apply IHs0.
  - reflexivity.
Qed.

(** Decide if a sort is equivalent to zero (does not recognize anything) *)

Open Scope bool.

Fixpoint is_zero s :=
  match s with
  | Zero => true
  | One | Name _ => false
  | Sum s1 s2 => is_zero s1 && is_zero s2
  | Prod s1 s2 => is_zero s1 || is_zero s2
  | Star _ => false
  end.

Lemma is_zero_correct : forall s,
    is_zero s = true <-> sem_sort s ≡ ∅.
Proof.
  induction s; simpl.
  - (* zero *)
    split; reflexivity.
  - (* one *)
    split; [intros ?; congruence|intros EQ; exfalso].
    specialize (EQ ∅n). apply EQ. autounfold with set. reflexivity.
  - (* name *)
    split; [intros ?; congruence|intros EQ; exfalso].
    specialize (EQ { x }n). apply EQ. autounfold with set. reflexivity.
  - (* sum *)
    rewrite Bool.andb_true_iff, IHs1, IHs2.
    split; [intros (Z1&Z2) m|intros ?]; autounfold with set.
    + split; [intros [S|S]|intros []]; [apply Z1 in S|apply Z2 in S]; auto.
    + split; (intros ?; split; [intros S|intros []]); apply H; [left|right]; auto.
  - (* prod *)
    rewrite Bool.orb_true_iff, IHs1, IHs2. autounfold with mset.
    split; [intros Z m|intros Z]; autounfold with set.
    + split; [intros (?&?&(?&?)&_)|intros []].
      destruct Z as [Z|Z]; eapply Z; eauto.
    + destruct (empty_or_not (sem_sort s1)) as [F|(m1&Ex1)].
      { left. split; [intros S|intros []]. apply F in S as []. }
      destruct (empty_or_not (sem_sort s2)) as [F|(m2&Ex2)].
      { right. split; [intros S|intros []]. apply F in S as []. }
      exfalso.
      apply (Z (fun x => m1 x + m2 x)). autounfold with set in *.
      repeat esplit; eauto.
  - (* star *)
    split; [intros ?; congruence|intros Z]. exfalso.
    apply (Z ∅n). exists 0. simpl. now autounfold with set.
Qed.

(** Empty intersection checker *)

(* Decompose disjunct expr *)
Function decompose_disjunct (s : sort_exp) :=
  match s with
  | Sum s1 s2 => List.app (decompose_disjunct s1) (decompose_disjunct s2)
  | _ => [s]
  end.

Lemma decompose_disjunct_complete : forall s,
    disjunctive s ->
    Forall no_disj (decompose_disjunct s).
Admitted.

Lemma decompose_disjunct_correct : forall s m,
    m ∈ sem_sort s <-> Exists (fun s => m ∈ sem_sort s) (decompose_disjunct s).
Proof.
  intros s. functional induction decompose_disjunct s.
  - intros. rewrite Exists_app, <-IHl, <-IHl0. reflexivity.
  - intros. rewrite Exists_cons, Exists_nil. firstorder.
Qed.

Function pick (s: sort_exp) :=
  match s with
  | Name x => Some (x, One)
  | Prod s1 s2 =>
      match pick s1, pick s2 with
      | Some (x, s1), _ => Some (x, Prod s1 s2)
      | None, Some (x, s2) => Some (x, Prod s1 s2)
      | None, None => None
      end
  | Sum s1 s2 =>
      match pick s1, pick s2 with
      | Some (x, s1), _ => Some (x, Sum s1 s2)
      | None, Some (x, s2) => Some (x, Sum s1 s2)
      | None, None => None
      end
  | _ => None
  end.

Fact pick_decreasing : forall s x s1,
    pick s = Some (x, s1) ->
    sort_size s1 < sort_size s.
Proof.
  intros ?.
  functional induction pick s; intros * EQ; inv EQ; simpl; auto.
  - specialize (IHo _ _ e0). lia.
  - specialize (IHo0 _ _ e1). lia.
  - specialize (IHo _ _ e0). lia.
  - specialize (IHo0 _ _ e1). lia.
Qed.

Function is_nonempty_inter_conj s1 s2 {measure sort_size s1} :=
  match is_zero s1, is_zero s2, pick s1 with
  | true, _, _ => false
  | _, true, _ => false
  | _, _, None => match pick s2 with None => true | _ => false end
  (* XXX the false in the above line is incorrect: we could be able to pick from s1,
     but there is no guarantee that the program would terminate *)
  | _, _, Some (x1, s1) => let s2 := derivative x1 s2 in is_nonempty_inter_conj s1 s2
  end.
Proof.
  intros * Z1 Z2 * PICK; subst.
  eapply pick_decreasing; eauto.
Qed.

Definition is_nonempty_inter s1 s2 :=
  let l1 := decompose_disjunct (make_disjunctive s1) in
  let l2 := decompose_disjunct (make_disjunctive s2) in
  existsb (fun s1 => existsb (is_nonempty_inter_conj s1) l2) l1.

Lemma pick_correct : forall s,
    is_zero s = false ->
    pick s = None <-> ∅n ∈ sem_sort s.
Proof.
  intros s. functional induction pick s; intros NZero.
  1-3,5-6:split; [intros ?; congruence|intros S; exfalso].
  6-8:split; [intros _|auto].
  all:simpl in *; autounfold with set mset in *.
  - (* name *)
    admit. (* OK *)
  - (* sum 1 *)
    admit.
  - (* sum 2 *)
    admit.
  - (* prod 1 *)
    admit.
  - (* prod 2 *)
    admit.
  - (* prod 3 *)
    admit.
  - (* sum 3 *)
    admit.
  - destruct s; inv y; simpl in *; try congruence.
    1,2:admit. (* OK *)
Admitted.

Lemma is_nonempty_inter_conj_correct : forall s1 s2,
    no_disj s1 ->
    is_nonempty_inter_conj s1 s2 = true <-> (exists m : mset name, m ∈ sem_sort s1 ∩ sem_sort s2).
Proof.
  intros ??. functional induction is_nonempty_inter_conj s1 s2; intros ND1.
  1,2,4:split; [intros; congruence|intros IN; exfalso].
  - apply is_zero_correct in e. admit. (* OK *)
  - apply is_zero_correct in e0. admit. (* OK *)
  - destruct (is_zero s1) eqn:Z1, (is_zero s2) eqn:Z2; inv y.
    destruct (pick s2) as [(?&?)|] eqn:P; inv y0.
    admit. (* contradict *)
  - split; [intros _|auto].
    destruct (is_zero s1) eqn:Z1, (is_zero s2) eqn:Z2; inv y.
    eapply pick_correct in e1; eauto. eapply pick_correct in e2; eauto.
    do 2 esplit; eauto.
  - admit.
Admitted.

Fact Exists_iff_In {A} : forall (P Q: A -> Prop) l,
    (forall x, In x l -> P x <-> Q x) ->
    Exists P l <-> Exists Q l.
Proof.
  intros * EQUIV.
  induction l.
  - now rewrite 2 Exists_nil.
  - rewrite 2 Exists_cons. rewrite EQUIV, IHl; auto with datatypes.
    reflexivity.
Qed.

Lemma is_nonempty_inter_correct : forall s1 s2,
    is_nonempty_inter s1 s2 = true <-> exists m, m ∈ (sem_sort s1 ∩ sem_sort s2).
Proof.
  intros.
  assert ((exists m, m ∈ sem_sort s1 ∩ sem_sort s2) <->
            Exists (fun s1 => Exists (fun s2 => exists m, m ∈ sem_sort s1 ∩ sem_sort s2)
                             (decompose_disjunct (make_disjunctive s2)))
              (decompose_disjunct (make_disjunctive s1))) as ->.
  { split.
    - intros (?&S1&S2).
      eapply make_disjunctive_correct, decompose_disjunct_correct in S1.
      eapply make_disjunctive_correct, decompose_disjunct_correct in S2.
      do 2 (eapply Exists_impl; [|eauto]; intros; simpl in * ).
      repeat esplit; eauto.
    - intros Ex. do 2 eapply Exists_exists in Ex as (?s&?In&Ex).
      destruct Ex as (m&S1&S2).
      do 2 esplit; eapply make_disjunctive_correct, decompose_disjunct_correct, Exists_exists; eauto.
  }

  unfold is_nonempty_inter.
  do 2 (rewrite existsb_exists, <-Exists_exists; apply Exists_iff_In; intros).

  apply is_nonempty_inter_conj_correct.
  - eapply Forall_forall, H.
    eapply decompose_disjunct_complete; eauto using make_disjunctive_complete.
  (* - eapply Forall_forall, H0. *)
  (*   eapply decompose_disjunct_complete; eauto using make_disjunctive_complete. *)
Qed.

(** Final compatibility checker *)

Definition is_compat s1 s2 :=
  (is_zero s1 && is_zero s2) || is_nonempty_inter s1 s2.

Theorem is_compat_correct : forall s1 s2,
    is_compat s1 s2 = true <-> s1 >< s2.
Proof.
  intros.
  unfold is_compat.
  rewrite compat_compat', Bool.orb_true_iff, Bool.andb_true_iff, 2 is_zero_correct.
  apply or_iff_compat_l.
  rewrite is_nonempty_inter_correct.
  split.
  - intros (?&IN). intros CONTRA. apply CONTRA in IN. eapply IN.
  - intros CONTRA.
    destruct (empty_or_not (sem_sort s1 ∩ sem_sort s2)) as [F|]; eauto.
    exfalso. apply CONTRA.
    split; [intros IN|intros []].
    apply F in IN as [].
Qed.
